package com.paradise.itsaquiz.di.module

import androidx.fragment.app.Fragment
import com.paradise.itsaquiz.db.dao.YesNoEntityDao
import com.paradise.itsaquiz.di.component.AppScope
import com.paradise.itsaquiz.di.fragment.DaggerFragmentFactory
import com.paradise.itsaquiz.di.fragment.FragmentKey
import com.paradise.itsaquiz.firestoreapi.YesNoApi
import com.paradise.itsaquiz.manager.AdManager
import com.paradise.itsaquiz.ui.fragments.ViewPagerFragment
import com.paradise.itsaquiz.ui.fragments.yesno.YesNoRootFragment
import com.paradise.itsaquiz.ui.fragments.yesno.details.YesNoDetailsFragment
import com.paradise.itsaquiz.ui.fragments.yesno.details.yesNoDetailsFeatureParams
import com.paradise.itsaquiz.ui.fragments.yesno.main.YesNoFragment
import com.paradise.itsaquiz.ui.fragments.yesno.main.yesNoFeatureParams
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Provider

@Module
class FragmentsBindingModule {

    @AppScope
    @Provides
    fun provideDaggerFragmentFactory(creators: Map<Class<out Fragment>, @JvmSuppressWildcards Provider<Fragment>>): DaggerFragmentFactory {
        return DaggerFragmentFactory(creators)
    }

    @Provides
    @IntoMap
    @FragmentKey(value = ViewPagerFragment::class)
    fun provideViewPagerFragment(factory: DaggerFragmentFactory): Fragment {
        return ViewPagerFragment(factory)
    }

    @Provides
    @IntoMap
    @FragmentKey(value = YesNoFragment::class)
    fun provideYesNoFragment(yesNoEntityDao: YesNoEntityDao, yesNoApi: YesNoApi, adManager: AdManager): Fragment {
        return YesNoFragment { yesNoFeatureParams(yesNoEntityDao, yesNoApi,adManager) }
    }

    @Provides
    @IntoMap
    @FragmentKey(value = YesNoDetailsFragment::class)
    fun provideYesNoDetailsFragment(adManager: AdManager): Fragment {
        return YesNoDetailsFragment { args -> yesNoDetailsFeatureParams(args, adManager) }
    }

    @Provides
    @IntoMap
    @FragmentKey(value = YesNoRootFragment::class)
    fun provideYesNoRootFragment(adManager: AdManager): Fragment {
        return YesNoRootFragment(adManager)
    }
}
