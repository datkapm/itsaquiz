package com.paradise.itsaquiz.di.component

import com.paradise.itsaquiz.di.module.*
import com.paradise.itsaquiz.ui.activity.SingleActivity
import dagger.Component
import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AppScope

@AppScope
@Component(modules = [AppModule::class, FragmentsBindingModule::class, RetrofitModule::class, AdModule::class, DaoModule::class])
interface AppComponent {

    fun inject(singleActivity: SingleActivity)
}
