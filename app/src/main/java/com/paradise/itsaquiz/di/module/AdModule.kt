package com.paradise.itsaquiz.di.module

import android.content.Context
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.RequestConfiguration
import com.paradise.itsaquiz.BuildConfig
import com.paradise.itsaquiz.di.component.AppScope
import com.paradise.itsaquiz.manager.AdManager
import dagger.Module
import dagger.Provides

@Module
class AdModule {

    @AppScope
    @Provides
    fun provideAdManager(context: Context, config: AdManager.Config): AdManager {
        MobileAds.initialize(context)

        if (BuildConfig.DEBUG) {
            MobileAds.setRequestConfiguration(
                RequestConfiguration.Builder().setTestDeviceIds(listOf("9D2AFA780BC5DA643DF98F1715C68367")).build()
            )
        }
        return AdManager(context, config)
    }

    @AppScope
    @Provides
    fun provideAdManagerConfig(): AdManager.Config = AdManager.Config(
        interstitialId = "ca-app-pub-9289016374634083/2989598645",
        bannerId = "ca-app-pub-9289016374634083/2114403193",
        timeInterval = 0,
        actionsToShowInterstitial = 0
    )
}
