package com.paradise.itsaquiz.di.module

import android.content.Context
import com.paradise.itsaquiz.db.ItsQuizDataBase
import com.paradise.itsaquiz.di.component.AppScope
import dagger.Module
import dagger.Provides

@Module
class AppModule(
    val context: Context,
    val dataBase: ItsQuizDataBase
) {

    @AppScope
    @Provides
    fun provideContext(): Context = context

    @AppScope
    @Provides
    fun provideItsQuizDataBase(): ItsQuizDataBase {
        return dataBase
    }

}
