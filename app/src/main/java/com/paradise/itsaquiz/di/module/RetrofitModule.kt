package com.paradise.itsaquiz.di.module

import com.google.gson.Gson
import com.paradise.itsaquiz.BuildConfig
import com.paradise.itsaquiz.db.converters.TimeStamp
import com.paradise.itsaquiz.di.component.AppScope
import com.paradise.itsaquiz.firestoreapi.YesNoApi
import com.paradise.itsaquiz.firestoreapi.yesNoTimeStampAdapterRetrofit
import com.paradise.itsaquiz.network.apiSpecs.YesNoApiSpec
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class RetrofitModule {

    companion object {
        const val baseUrl: String = "http://37.1.202.158:35003"
    }

    @AppScope
    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(
                GsonConverterFactory.create(
                    Gson().newBuilder()
                        .registerTypeAdapter(TimeStamp::class.java, yesNoTimeStampAdapterRetrofit)
                        .create()
                )
            )
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
                    })
                    .build()
            ).build()
    }

    @AppScope
    @Provides
    fun provideYesNoApi(retrofit: Retrofit): YesNoApi {
        return YesNoApi(retrofit.create(YesNoApiSpec::class.java))
    }
}