package com.paradise.itsaquiz.di.module

import com.paradise.itsaquiz.db.dao.YesNoEntityDao
import com.paradise.itsaquiz.di.component.AppScope
import dagger.Module
import dagger.Provides

@Module
class DaoModule(private val yesNoEntityDao: YesNoEntityDao) {

    @AppScope
    @Provides
    fun provideYesNoEntityDao(): YesNoEntityDao = yesNoEntityDao

}
