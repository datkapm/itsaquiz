package com.paradise.itsaquiz.network.apiSpecs

import com.paradise.itsaquiz.db.dao.YesNoEntity
import retrofit2.http.GET
import retrofit2.http.Query

interface YesNoApiSpec {
    @GET("/api/cards")
    suspend fun fetchYesNoCards(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int,
        @Query("lang") language: String? = null
    ): List<YesNoEntity>
}