package com.paradise.itsaquiz.manager

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.ViewGroup
import com.google.android.gms.ads.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class AdManager(context: Context, private val config: Config) : CoroutineScope by CoroutineScope(Dispatchers.Main) {

    private val preferences: SharedPreferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)

    private val interstitialAd: InterstitialAd =
        InterstitialAd(context).apply {
            adUnitId = config.interstitialId
            loadAd(AdRequest.Builder().build())
            adListener = object : AdListener() {
                override fun onAdClosed() {
                    if (!isLoading && !isLoaded) loadAd(AdRequest.Builder().build())
                }
            }
        }

    companion object {
        const val APP_PREFERENCES = "settings"
        private const val ACTION_COUNT_KEY = "com.paradise.itsquiz.actionCountKey"
    }

    fun showBanner(adViewContainer: ViewGroup) {
        val adView = AdView(adViewContainer.context).apply {
            adSize = AdSize.SMART_BANNER
            adUnitId = config.bannerId
            loadAd(AdRequest.Builder().build())

            adListener = object : AdListener() {
                override fun onAdFailedToLoad(p0: LoadAdError?) {
                    Log.d("AdManager", "error code = ${p0?.code}, message = ${p0?.message}")
                    super.onAdFailedToLoad(p0)
                }
            }
        }

        adViewContainer.addView(adView)
    }

    fun showInterstitialIfNeed() {
        launch {
            with(interstitialAd) {
                if (isLoaded) {
                    val count = preferences.getInt(ACTION_COUNT_KEY, 0)
                    if (count >= 5) {
                        preferences.edit().putInt(ACTION_COUNT_KEY, 0).apply()
                        show()
                    } else {
                        preferences.edit().putInt(ACTION_COUNT_KEY, count + 1).apply()
                    }
                }
            }
        }
    }

    data class Config(
        val interstitialId: String,
        val bannerId: String,
        val timeInterval: Int,
        val actionsToShowInterstitial: Int
    )
}