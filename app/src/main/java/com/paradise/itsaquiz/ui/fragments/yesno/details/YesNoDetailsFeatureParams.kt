package com.paradise.itsaquiz.ui.fragments.yesno.details

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.ColorRes
import com.paradise.itsaquiz.manager.AdManager
import com.paradise.itsaquiz.tea.Connector
import com.paradise.itsaquiz.tea.feature.FeatureParams
import com.paradise.itsaquiz.ui.fragments.yesno.main.YesNoAdapter
import kotlinx.android.parcel.Parceler
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.WriteWith
import oolong.effect
import oolong.effect.none

typealias YesNoDetailsFeatureParams = FeatureParams<YesNoDetailsModel, YesNoDetailsMsg, YesNoDetailsProps>
typealias YesNoDetailsConnector = Connector<YesNoDetailsModel, YesNoDetailsMsg, YesNoDetailsProps>

fun yesNoDetailsFeatureParams(args: YesNoDetailsFragmentArgs, adManager: AdManager): YesNoDetailsFeatureParams = FeatureParams(
    init = { previous ->
        val model = previous ?: YesNoDetailsModel(
            yesNoCard = args.yesNoCard,
            navigationActionAction = null
        )
        model to none()
    },
    update = { msg, model ->

        when (msg) {
            is YesNoDetailsMsg.ShowAnswer -> model.copy(navigationActionAction = YesNoNavigationAction.ShowAnswerDialog(model.yesNoCard.answer)) to
                    effect { adManager.showInterstitialIfNeed() }
            is YesNoDetailsMsg.GoBack -> model.copy(navigationActionAction = YesNoNavigationAction.Back) to none()
            is YesNoDetailsMsg.CompleteNavigation -> model.copy(navigationActionAction = null) to none()
        }
    },
    view = { model ->
        YesNoDetailsProps(
            image = model.yesNoCard.image,
            title = model.yesNoCard.title,
            question = model.yesNoCard.question,
            answer = model.yesNoCard.answer,
            averageTime = model.yesNoCard.averageTime,
            difficultyColor = model.yesNoCard.difficultyColor,
            navigationActionActionConsumer = model.navigationActionAction?.let { navigation ->
                { connector: YesNoDetailsConnector -> connector.dispatch(YesNoDetailsMsg.CompleteNavigation); navigation }
            }
        )
    }

)

@Parcelize
data class YesNoDetailsModel(
    val yesNoCard: YesNoAdapter.YesNo.Card,
    val navigationActionAction: @WriteWith<IgnoreNavigationActionParceler> YesNoNavigationAction?
) : Parcelable

private object IgnoreNavigationActionParceler : Parceler<YesNoNavigationAction?> {
    override fun create(parcel: Parcel): YesNoNavigationAction? = null
    override fun YesNoNavigationAction?.write(parcel: Parcel, flags: Int) = Unit
}

sealed class YesNoDetailsMsg {
    object ShowAnswer : YesNoDetailsMsg()
    object CompleteNavigation : YesNoDetailsMsg()
    object GoBack : YesNoDetailsMsg()
}

data class YesNoDetailsProps(
    val image: String,
    val title: String,
    val question: String,
    val answer: String,
    val averageTime: Int,
    @ColorRes val difficultyColor: Int,
    val navigationActionActionConsumer: ((YesNoDetailsConnector) -> YesNoNavigationAction)?
)

sealed class YesNoNavigationAction {
    object Back : YesNoNavigationAction()
    data class ShowAnswerDialog(val answer: String) : YesNoNavigationAction()
}
