package com.paradise.itsaquiz.ui.fragments.yesno.details

import android.content.res.ColorStateList
import android.os.Bundle
import android.os.Parcelable
import android.transition.TransitionInflater
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.bundleOf
import androidx.core.view.ViewCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import com.paradise.itsaquiz.R
import com.paradise.itsaquiz.common.extension.throttleFirst
import com.paradise.itsaquiz.common.extension.transitionNameCompat
import com.paradise.itsaquiz.databinding.FragmentYesNoDetailsBinding
import com.paradise.itsaquiz.tea.androidConnectors
import dev.chrisbanes.insetter.doOnApplyWindowInsets
import kotlinx.android.parcel.Parcelize
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull

class YesNoDetailsFragment(
    private val featureProvider: (args: YesNoDetailsFragmentArgs) -> YesNoDetailsFeatureParams
) : Fragment(R.layout.fragment_yes_no_details) {

    private val binding: FragmentYesNoDetailsBinding by viewBinding()
    private val args: YesNoDetailsFragmentArgs by navArgs()

    private val connector: YesNoDetailsConnector by androidConnectors(featureParams = { featureProvider(args) })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val transition = TransitionInflater.from(context).inflateTransition(R.transition.yes_no_card_detail_transition)

        sharedElementReturnTransition = transition
        sharedElementEnterTransition = transition
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postponeEnterTransition()
        setupTransitionsName(args.transitions)

        with(binding) {
            closeButton.setOnClickListener { connector.dispatch(YesNoDetailsMsg.GoBack) }
            showAnswerButton.setOnClickListener { connector.dispatch(YesNoDetailsMsg.ShowAnswer) }

            rootContainerScrollView.doOnApplyWindowInsets { element, insets, initialState ->
                element.updatePadding(top = initialState.paddings.top + insets.systemWindowInsetTop)
            }
            containerConstraintLayout.doOnApplyWindowInsets { element, insets, initialState ->
                element.updatePadding(bottom = initialState.paddings.bottom + insets.systemWindowInsetBottom)
                startPostponedEnterTransition()
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            connector.props
                .mapNotNull { it.navigationActionActionConsumer }
                .map { it(connector) }
                .throttleFirst(1000)
                .collect { navigationAction -> consumeNavigation(navigationAction) }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            connector.props.distinctUntilChanged().collect { props: YesNoDetailsProps -> render(props) }
        }
    }

    private fun setupTransitionsName(transitions: YesNoDetailTransition) {
        with(binding) {
            imageView.transitionNameCompat = transitions.image
            titleTextView.transitionNameCompat = transitions.title
            averageTimeTextView.transitionNameCompat = transitions.time
            difficultyView.transitionNameCompat = transitions.difficulty
        }
    }

    private fun consumeNavigation(navigationAction: YesNoNavigationAction) {
        when (navigationAction) {
            YesNoNavigationAction.Back -> {
                setFragmentResult(DETAIL_RESULT_REQUEST_KEY, bundleOf())
                findNavController().popBackStack()
            }
            is YesNoNavigationAction.ShowAnswerDialog ->
                findNavController().navigate(
                    YesNoDetailsFragmentDirections.navigateToYesNoDetailsAnswerDialogFragment(
                        getString(R.string.dialog_fragment_answer), navigationAction.answer
                    )
                )
        }
    }

    private fun render(props: YesNoDetailsProps) {
        with(binding) {
            averageTimeTextView.text = root.resources.getString(R.string.averageTime, props.averageTime)
            titleTextView.text = props.title
            questionTextView.text = props.question

            Glide.with(root.context).load(props.image).into(imageView)

            ViewCompat.setBackgroundTintList(
                difficultyView,
                ColorStateList.valueOf(ResourcesCompat.getColor(root.context.resources, props.difficultyColor, root.context.theme))
            )
        }
    }

    @Parcelize
    data class YesNoDetailTransition(
        val image: String,
        val time: String,
        val title: String,
        val difficulty: String
    ) : Parcelable

    enum class YesNoDetailTransitionElements { IMAGE, TIME, TITLE, DIFFICULTY }

    companion object {
        const val DETAIL_RESULT_REQUEST_KEY = "com.paradise.itsaquiz.detail_request_key"
    }
}
