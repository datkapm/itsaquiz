package com.paradise.itsaquiz.ui.fragments

import android.app.Dialog
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.paradise.itsaquiz.common.extension.findRootNavController
import com.paradise.itsaquiz.databinding.FragmentNoInternetDialogFramgneBinding

class NoInternetDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val binding = FragmentNoInternetDialogFramgneBinding.inflate(layoutInflater, null, false)
        binding.closeAppCompatButton.setOnClickListener { findRootNavController().popBackStack() }
        binding.refreshAppCompatButton.setOnClickListener {
            setFragmentResult(ERROR_REQUEST_KEY, bundleOf(BUNDLE_IS_ERROR to true))
            findNavController().popBackStack()
        }

        return MaterialAlertDialogBuilder(requireContext())
            .setView(binding.root)
            .create().apply { isCancelable = false }
    }

    companion object {
        const val ERROR_REQUEST_KEY = "com.paradise.itsaquiz.ui.error_request_key"
        const val BUNDLE_IS_ERROR = "com.paradise.itsaquiz.ui.isError"
    }
}
