package com.paradise.itsaquiz.ui.fragments.yesno.main

import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.paradise.itsaquiz.R
import com.paradise.itsaquiz.common.ParcelableResponse
import com.paradise.itsaquiz.db.dao.YesNoEntity
import com.paradise.itsaquiz.db.dao.YesNoEntityDao
import com.paradise.itsaquiz.firestoreapi.YesNoApi
import com.paradise.itsaquiz.manager.AdManager
import com.paradise.itsaquiz.tea.Connector
import com.paradise.itsaquiz.tea.feature.FeatureParams
import kotlinx.android.parcel.Parceler
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.WriteWith
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import oolong.effect
import oolong.effect.batch
import oolong.effect.none

typealias YesNoFeatureParams = FeatureParams<YesNoModel, YesNoMsg, YesNoProps>
typealias YesNoConnector = Connector<YesNoModel, YesNoMsg, YesNoProps>

fun yesNoFeatureParams(
    yesNoEntityDao: YesNoEntityDao,
    yesNoApi: YesNoApi,
    adManager: AdManager
): YesNoFeatureParams = FeatureParams(
    init = { previous ->
        val model = previous ?: YesNoModel(
            entityList = emptyList(),
            isShowPlaceHolders = false,
            lastLoadingRequest = LastLoadingRequest(0, false),
            paginationStep = 20,
            navigationAction = null
        )
        model to batch(
            effect { dispatch ->
                yesNoEntityDao.getAll()
                    .distinctUntilChanged()
                    .filter { it.isNotEmpty() }
                    .collect { dispatch(InternalMsg.FetchData(it)) }
            },
            effect { dispatch ->
                delay(150)
                dispatch(InternalMsg.LoadAdditionalData)
            }
        )
    },
    update = { msg, model ->
        when (msg) {
            is InternalMsg -> when (msg) {
                is InternalMsg.FetchData -> model.copy(entityList = msg.entityList) to none()
                is InternalMsg.SaveData -> model.copy(
                    lastLoadingRequest = model.lastLoadingRequest.copy(
                        isLoaded = msg.isNeedToLoad,
                        lastReceivedPosition = model.lastLoadingRequest.lastReceivedPosition + model.paginationStep
                    )
                ) to effect {
                    yesNoEntityDao.saveYesNoEntityCards(
                        offset = model.lastLoadingRequest.lastReceivedPosition,
                        limit = model.paginationStep,
                        yesNoEntityList = msg.entityList
                    )
                }
                InternalMsg.LoadAdditionalData -> model.copy(lastLoadingRequest = model.lastLoadingRequest.copy(isLoaded = false)) to effect { dispatch ->
                    Log.d("AOP", "LoadMoreAfter = ${model.lastLoadingRequest.lastReceivedPosition}")
                    dispatch(InternalMsg.FetchPlaceHolderStatus(true))
                    when (val response = yesNoApi.getNextPageData(model.lastLoadingRequest.lastReceivedPosition, model.paginationStep)) {
                        is ParcelableResponse.Success -> if (response.value.isNotEmpty()) {
                            dispatch(InternalMsg.SaveData(response.value, response.value.size == model.paginationStep))
                        }
                        is ParcelableResponse.Failure -> if (model.entityList.isEmpty()) dispatch(ExternalMsg.Navigate(NavigationAction.OpenErrorDialog))
                    }
                    dispatch(InternalMsg.FetchPlaceHolderStatus(false))
                }
                is InternalMsg.FetchPlaceHolderStatus -> model.copy(isShowPlaceHolders = msg.isShow) to none()
                is InternalMsg.CompleteNavigation -> model.copy(navigationAction = null) to none()

            }
            is ExternalMsg -> when (msg) {
                is ExternalMsg.LoadData -> model to effect { dispatch ->
                    if (model.lastLoadingRequest.isLoaded && model.lastLoadingRequest.lastReceivedPosition - msg.lastVisiblePosition <= 10) dispatch(
                        InternalMsg.LoadAdditionalData
                    )
                }
                ExternalMsg.Refresh -> model to effect { dispatch -> dispatch(InternalMsg.LoadAdditionalData) }
                is ExternalMsg.Navigate -> model.copy(navigationAction = msg.action) to effect {
                    adManager.showInterstitialIfNeed()
                    Log.d("AOP", "msg ${msg.action}")
                }
            }
            else -> error("Unknown msg = $msg")
        }
    },
    view = { model ->
        val cardList = model.entityList.map { entity -> entity.mapToYesNoCard() }
        YesNoProps(
            cardList = when {
                cardList.isEmpty() && model.isShowPlaceHolders -> cardList.addPlaceHolders()
                cardList.isNotEmpty() && model.isShowPlaceHolders
                    && model.lastLoadingRequest.lastReceivedPosition / cardList.size == 1 -> cardList.addPlaceHolders()
                else -> cardList
            },
            navigateActionConsumer = model.navigationAction?.let { navigation ->
                { connector -> connector.dispatch(InternalMsg.CompleteNavigation); navigation }
            }
        )
    })

private fun List<YesNoAdapter.YesNo>.addPlaceHolders() =
    this.plus(List(size = 9) { it }.map { YesNoAdapter.YesNo.PlaceHolder })

private fun YesNoEntity.mapToYesNoCard(): YesNoAdapter.YesNo.Card =
    YesNoAdapter.YesNo.Card(
        id = this.id,
        answer = this.answer,
        averageTime = this.averageTime,
        difficultyColor = this.difficultyColor(),
        image = this.image,
        lastModified = this.lastModified,
        question = this.question,
        title = this.title,
        isLocked = isUnlocked
    )

fun YesNoEntity.difficultyColor(): Int = when (difficulty) {
    YesNoEntity.Difficulty.EASY.name -> R.color.greenYellow
    YesNoEntity.Difficulty.MEDIUM.name -> R.color.ronchi
    YesNoEntity.Difficulty.HARD.name -> R.color.carnation
    else -> error("Unknown Difficulty $difficulty")
}

@Parcelize
data class YesNoModel(
    val entityList: List<YesNoEntity>,
    val isShowPlaceHolders: Boolean,
    val lastLoadingRequest: LastLoadingRequest,
    val paginationStep: Int,
    val navigationAction: @WriteWith<IgnoreNavigationActionParceler> NavigationAction?
) : Parcelable

private object IgnoreNavigationActionParceler : Parceler<NavigationAction?> {
    override fun create(parcel: Parcel): NavigationAction? = null
    override fun NavigationAction?.write(parcel: Parcel, flags: Int) = Unit
}

@Parcelize
data class LastLoadingRequest(
    val lastReceivedPosition: Int,
    val isLoaded: Boolean
) : Parcelable

interface YesNoMsg

private sealed class InternalMsg : YesNoMsg {
    data class FetchData(val entityList: List<YesNoEntity>) : InternalMsg()
    data class SaveData(val entityList: List<YesNoEntity>, val isNeedToLoad: Boolean) : InternalMsg()
    object CompleteNavigation : InternalMsg()

    object LoadAdditionalData : InternalMsg()
    data class FetchPlaceHolderStatus(val isShow: Boolean) : InternalMsg()
}

sealed class ExternalMsg : YesNoMsg {
    object Refresh : ExternalMsg()
    data class Navigate(val action: NavigationAction) : ExternalMsg()
    data class LoadData(val lastVisiblePosition: Int) : ExternalMsg()
}

data class YesNoProps(
    val cardList: List<YesNoAdapter.YesNo>,
    val navigateActionConsumer: ((connector: YesNoConnector) -> NavigationAction)?
)

sealed class NavigationAction {
    object OpenErrorDialog : NavigationAction()
    data class OpenCardDetail(
        val card: YesNoAdapter.YesNo.Card, val transitionInfo: TransitionInfo
    ) : NavigationAction()
}