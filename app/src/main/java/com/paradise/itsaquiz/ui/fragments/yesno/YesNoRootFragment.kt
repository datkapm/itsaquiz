package com.paradise.itsaquiz.ui.fragments.yesno

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.paradise.itsaquiz.R
import com.paradise.itsaquiz.databinding.FragmentYesNoRootBinding
import com.paradise.itsaquiz.manager.AdManager
import dev.chrisbanes.insetter.doOnApplyWindowInsets

class YesNoRootFragment(private val adManager: AdManager) : Fragment(R.layout.fragment_yes_no_root) {
    private val binding: FragmentYesNoRootBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.adViewContainer.doOnApplyWindowInsets { element, insets, initialState ->
            element.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                bottomMargin = initialState.paddings.bottom + insets.systemWindowInsetBottom
            }
        }

        adManager.showBanner(binding.adViewContainer)
    }

}