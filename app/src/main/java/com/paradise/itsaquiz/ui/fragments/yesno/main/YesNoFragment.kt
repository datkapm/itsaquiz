package com.paradise.itsaquiz.ui.fragments.yesno.main

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.doOnPreDraw
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.play.core.review.ReviewInfo
import com.google.android.play.core.review.ReviewManagerFactory
import com.paradise.itsaquiz.R
import com.paradise.itsaquiz.common.GlideApp
import com.paradise.itsaquiz.common.extension.throttleFirst
import com.paradise.itsaquiz.common.extension.transitionNameCompat
import com.paradise.itsaquiz.databinding.FragmentYesNoBinding
import com.paradise.itsaquiz.databinding.ItemYesNoBinding
import com.paradise.itsaquiz.databinding.ItemYesNoPlaceholderBinding
import com.paradise.itsaquiz.db.converters.TimeStamp
import com.paradise.itsaquiz.tea.androidConnectors
import com.paradise.itsaquiz.ui.fragments.NoInternetDialogFragment
import com.paradise.itsaquiz.ui.fragments.yesno.details.YesNoDetailsFragment
import dev.chrisbanes.insetter.doOnApplyWindowInsets
import kotlinx.android.parcel.Parcelize
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.launch

typealias TransitionViewNames = Pair<View, String>
typealias TransitionInfo = MutableMap<String, TransitionViewNames>

class YesNoFragment(private val featureProvider: () -> YesNoFeatureParams) : Fragment(R.layout.fragment_yes_no) {

    private val binding: FragmentYesNoBinding by viewBinding()
    private val yesNoAdapter: YesNoAdapter by lazy(LazyThreadSafetyMode.NONE) {
        YesNoAdapter(
            onCardClick = { yesNoCard, transitions ->
                connector.dispatch(ExternalMsg.Navigate(NavigationAction.OpenCardDetail(yesNoCard, transitions)))
            }
        )
    }
    private val connector: YesNoConnector by androidConnectors(featureParams = { featureProvider() })

    private val reviewManager by lazy(LazyThreadSafetyMode.NONE) { ReviewManagerFactory.create(requireContext()) }
    private var reviewInfo: ReviewInfo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        reviewManager.requestReviewFlow().addOnSuccessListener { reviewInfo = it }

        setFragmentResultListener(requestKey = NoInternetDialogFragment.ERROR_REQUEST_KEY)
        { requestKey, bundle ->
            when {
                requestKey == NoInternetDialogFragment.ERROR_REQUEST_KEY
                    && bundle.getBoolean(NoInternetDialogFragment.BUNDLE_IS_ERROR) -> connector.dispatch(ExternalMsg.Refresh)
            }
        }
        setFragmentResultListener(YesNoDetailsFragment.DETAIL_RESULT_REQUEST_KEY)
        { _, _ -> tryShowInAppReview() }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        container?.doOnPreDraw { startPostponedEnterTransition() }
        postponeEnterTransition()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding.recyclerView) {
            val gridLayoutManager = GridLayoutManager(this.context, 3)
            doOnApplyWindowInsets { view, insets, initialState ->
                view.updatePadding(bottom = insets.systemWindowInsetBottom + initialState.paddings.bottom)
            }

            adapter = yesNoAdapter
            layoutManager = gridLayoutManager

            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    val lastVisiblePosition: Int = gridLayoutManager.findLastCompletelyVisibleItemPosition()
                    yesNoAdapter.currentList.lastOrNull()?.let { connector.dispatch(ExternalMsg.LoadData(lastVisiblePosition)) }
                }
            })
        }
        viewLifecycleOwner.lifecycleScope.launch {
            connector.props.map { it.cardList }
                .distinctUntilChanged()
                .collect { render(it) }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            connector.props.mapNotNull { it.navigateActionConsumer }
                .map { it(connector) }
                .throttleFirst(500)
                .collect { navigationAction ->
                    when (navigationAction) {
                        NavigationAction.OpenErrorDialog -> findNavController().navigate(YesNoFragmentDirections.navigateToErrorDialog())
                        is NavigationAction.OpenCardDetail -> findNavController().navigate(
                            YesNoFragmentDirections.navigateToYesNoDetailsFragment(
                                navigationAction.card, YesNoDetailsFragment.YesNoDetailTransition(
                                    checkNotNull(navigationAction.transitionInfo[YesNoDetailsFragment.YesNoDetailTransitionElements.IMAGE.name]).second,
                                    checkNotNull(navigationAction.transitionInfo[YesNoDetailsFragment.YesNoDetailTransitionElements.TIME.name]).second,
                                    checkNotNull(navigationAction.transitionInfo[YesNoDetailsFragment.YesNoDetailTransitionElements.TITLE.name]).second,
                                    checkNotNull(navigationAction.transitionInfo[YesNoDetailsFragment.YesNoDetailTransitionElements.DIFFICULTY.name]).second
                                )
                            ),
                            FragmentNavigator.Extras.Builder()
                                .addSharedElements(navigationAction.transitionInfo.entries.map { it.value.first to it.value.second }
                                    .toMap())
                                .build()
                        )

                    }
                }
        }
    }

    private fun render(cardList: List<YesNoAdapter.YesNo>): Unit = yesNoAdapter.submitList(cardList)

    private fun tryShowInAppReview() = reviewInfo?.let { reviewManager.launchReviewFlow(activity, it) }
}

class YesNoAdapter(val onCardClick: (card: YesNo.Card, transition: TransitionInfo) -> Unit) :
    ListAdapter<YesNoAdapter.YesNo, YesNoAdapter.YesNoViewHolder>(this) {

    override fun getItemViewType(position: Int): Int {
        return when (currentList[position]) {
            YesNo.PlaceHolder -> R.layout.item_yes_no_placeholder
            is YesNo.Card -> R.layout.item_yes_no
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): YesNoViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            R.layout.item_yes_no -> YesNoViewHolder.YesNoCardViewHolder(
                ItemYesNoBinding.inflate(inflater), onCardClick
            )
            R.layout.item_yes_no_placeholder -> YesNoViewHolder.YesNoPlaceHolderViewHolder(
                ItemYesNoPlaceholderBinding.inflate(inflater)
            )
            else -> error("Unknown item view type $viewType")
        }
    }

    override fun onBindViewHolder(holder: YesNoViewHolder, position: Int) {
        holder.render(currentList[position])
    }

    sealed class YesNoViewHolder(binding: ViewBinding) : RecyclerView.ViewHolder(binding.root) {

        abstract fun render(card: YesNo)

        fun addUniqueTransitionNameToMap(transitionName: String, view: View, sharedElementsMap: TransitionInfo): String {
            val uniqueTransitionName = "name = ${transitionName}, position = $adapterPosition"
            sharedElementsMap[transitionName] = view to uniqueTransitionName
            return uniqueTransitionName
        }

        class YesNoCardViewHolder(
            private val binding: ItemYesNoBinding,
            private val onCardClick: (card: YesNo.Card, transition: TransitionInfo) -> Unit
        ) : YesNoViewHolder(binding) {

            private val sharedElements = mutableMapOf<String, TransitionViewNames>()

            override fun render(card: YesNo) {
                card as YesNo.Card

                with(binding) {

                    GlideApp.with(root)
                        .load(card.image)
                        .thumbnail(0.30f)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                        .into(imageView)

                    imageView.transitionNameCompat =
                        addUniqueTransitionNameToMap(
                            YesNoDetailsFragment.YesNoDetailTransitionElements.IMAGE.name,
                            imageView,
                            sharedElements
                        )
                    averageTimeTextView.transitionNameCompat =
                        addUniqueTransitionNameToMap(
                            YesNoDetailsFragment.YesNoDetailTransitionElements.TIME.name,
                            averageTimeTextView,
                            sharedElements
                        )

                    titleTextView.transitionNameCompat =
                        addUniqueTransitionNameToMap(
                            YesNoDetailsFragment.YesNoDetailTransitionElements.TITLE.name,
                            titleTextView,
                            sharedElements
                        )
                    difficultyView.transitionNameCompat =
                        addUniqueTransitionNameToMap(
                            YesNoDetailsFragment.YesNoDetailTransitionElements.DIFFICULTY.name,
                            difficultyView,
                            sharedElements
                        )

                    titleTextView.text = card.title
                    averageTimeTextView.text = root.resources.getString(R.string.averageTime, card.averageTime)
                    with(ResourcesCompat.getColor(root.resources, card.difficultyColor, root.context.theme)) {
                        infoContainerLinearLayout.setBackgroundColor(this)
                        difficultyView.setBackgroundColor(this)
                    }
                    binding.root.setOnClickListener { onCardClick.invoke(card, sharedElements) }
                }
            }
        }

        class YesNoPlaceHolderViewHolder(private val binding: ItemYesNoPlaceholderBinding) : YesNoViewHolder(binding) {
            override fun render(card: YesNo) {
                binding.bodyShimmer.startShimmer()
            }
        }
    }

    companion object : DiffUtil.ItemCallback<YesNo>() {
        override fun areItemsTheSame(oldItem: YesNo, newItem: YesNo): Boolean {
            return when {
                oldItem is YesNo.Card && newItem is YesNo.Card -> oldItem.id == newItem.id
                oldItem is YesNo.PlaceHolder && newItem is YesNo.PlaceHolder -> true
                else -> false
            }
        }

        override fun areContentsTheSame(oldItem: YesNo, newItem: YesNo): Boolean {
            return when {
                oldItem is YesNo.Card && newItem is YesNo.Card -> oldItem.lastModified == newItem.lastModified
                oldItem is YesNo.PlaceHolder && newItem is YesNo.PlaceHolder -> oldItem == newItem
                else -> false
            }
        }
    }

    sealed class YesNo {

        object PlaceHolder : YesNo()

        @Parcelize
        data class Card(
            val id: Int,
            val title: String,
            val question: String,
            val answer: String,
            val averageTime: Int,
            @ColorRes val difficultyColor: Int,
            val image: String,
            val lastModified: TimeStamp,
            val isLocked: Boolean
        ) : YesNo(), Parcelable
    }
}
