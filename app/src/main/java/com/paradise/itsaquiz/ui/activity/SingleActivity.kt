package com.paradise.itsaquiz.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavHost
import androidx.navigation.fragment.NavHostFragment
import com.paradise.itsaquiz.App
import com.paradise.itsaquiz.R
import com.paradise.itsaquiz.di.fragment.DaggerFragmentFactory
import dev.chrisbanes.insetter.setEdgeToEdgeSystemUiFlags
import kotlinx.android.synthetic.main.activity_single.*
import javax.inject.Inject

class SingleActivity : AppCompatActivity(R.layout.activity_single), NavHost {

    @Inject
    lateinit var daggerFragmentFactory: DaggerFragmentFactory

    private val innerNavController: NavController by lazy(LazyThreadSafetyMode.NONE) {
        requireNotNull(supportFragmentManager.findFragmentById(R.id.activityRootNavigationHost) as? NavHostFragment).navController
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        (application as App).appComponent.inject(this)
        supportFragmentManager.fragmentFactory = daggerFragmentFactory
        super.onCreate(savedInstanceState)
        singleActivityRootMainContainer.setEdgeToEdgeSystemUiFlags(enabled = true)
    }

    override fun getNavController(): NavController = innerNavController
}
