package com.paradise.itsaquiz.ui.fragments.yesno

import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.github.satoshun.coroutinebinding.view.clicks
import com.paradise.itsaquiz.R
import com.paradise.itsaquiz.common.extension.collectWithLifecycle
import com.paradise.itsaquiz.common.extension.doOnAnimation
import com.paradise.itsaquiz.common.extension.throttleFirst
import com.paradise.itsaquiz.databinding.FragmentYesNoPresentationBinding
import com.paradise.itsaquiz.ui.fragments.ViewPagerFragmentDirections
import dev.chrisbanes.insetter.doOnApplyWindowInsets
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.receiveAsFlow

class YesNoPresentationFragment : Fragment(R.layout.fragment_yes_no_presentation) {
    private val binding: FragmentYesNoPresentationBinding by viewBinding()
    private var isAnimationShown: Boolean = false

    @OptIn(ExperimentalCoroutinesApi::class)
    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.mainFrameLayoutContainer.doOnApplyWindowInsets { element, insets, initialState ->
            element.updatePadding(
                top = initialState.paddings.top + insets.systemWindowInsetTop,
                bottom = initialState.paddings.bottom + insets.systemWindowInsetBottom
            )
        }
        if (isAnimationShown.not()) {
            binding.containerConstraintLayout.startAnimation(
                AnimationUtils.loadAnimation(requireContext(), R.anim.fade_in)
                    .doOnAnimation(doOnEnd = {
                        isAnimationShown = true
                        binding.containerConstraintLayout.visibility = View.VISIBLE
                    })
            )
        } else {
            binding.containerConstraintLayout.visibility = View.VISIBLE
        }

        merge(binding.playButton.clicks().receiveAsFlow().map { Action.PLAY_GAME },
            binding.rulesButton.clicks().receiveAsFlow().map { Action.SHOW_RULES })
            .throttleFirst(500)
            .collectWithLifecycle(viewLifecycleOwner) {
                when (it) {
                    Action.PLAY_GAME -> findNavController().navigate(ViewPagerFragmentDirections.navigateToYesNoFragment())
                    Action.SHOW_RULES -> findNavController().navigate(
                        ViewPagerFragmentDirections.navigateToRules(
                            getString(R.string.rules),
                            getString(R.string.fragment_yes_no_rules)
                        )
                    )
                }
            }
    }

    private enum class Action { PLAY_GAME, SHOW_RULES }
}
