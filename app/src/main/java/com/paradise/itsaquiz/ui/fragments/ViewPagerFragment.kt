package com.paradise.itsaquiz.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import by.kirich1409.viewbindingdelegate.viewBinding
import com.paradise.itsaquiz.R
import com.paradise.itsaquiz.databinding.FragmentViewPagerBinding
import com.paradise.itsaquiz.di.fragment.DaggerFragmentFactory
import com.paradise.itsaquiz.ui.fragments.crocodile.CrocodilePresentationFragment
import com.paradise.itsaquiz.ui.fragments.yesno.YesNoPresentationFragment
import kotlin.reflect.KClass

class ViewPagerFragment(val fragmentFactory: DaggerFragmentFactory) : Fragment(R.layout.fragment_view_pager) {

    private val binding: FragmentViewPagerBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {

            viewPager.adapter = object : FragmentStateAdapter(this@ViewPagerFragment) {
                override fun getItemCount(): Int = PageAdapterFragments.values().size
                override fun createFragment(position: Int): Fragment =
                    fragmentFactory.instantiate(
                        requireContext().classLoader, PageAdapterFragments.values()[position].fragmentClass.java.name
                    )
            }

            requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
                if (viewPager.currentItem > 0) viewPager.currentItem = viewPager.currentItem.dec() else requireActivity().finish()
            }

        }
    }

    enum class PageAdapterFragments(val fragmentClass: KClass<out Fragment>) {
        FIRST(YesNoPresentationFragment::class),
        SECOND(CrocodilePresentationFragment::class)
    }
}
