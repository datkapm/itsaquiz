package com.paradise.itsaquiz.ui.fragments.crocodile

import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.paradise.itsaquiz.R
import com.paradise.itsaquiz.common.extension.doOnAnimation
import com.paradise.itsaquiz.databinding.FragmentCrocodilePresentationBinding
import dev.chrisbanes.insetter.doOnApplyWindowInsets

class CrocodilePresentationFragment : Fragment(R.layout.fragment_crocodile_presentation) {
    private val binding: FragmentCrocodilePresentationBinding by viewBinding()
    private var isAnimationShown: Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.mainConstrainLayoutContainer.doOnApplyWindowInsets { element, insets, initialState ->
            element.updatePadding(
                top = initialState.paddings.top + insets.systemWindowInsetTop,
                bottom = initialState.paddings.bottom + insets.systemWindowInsetBottom
            )
        }

        if (isAnimationShown.not()) {
            binding.mainConstrainLayoutContainer.startAnimation(
                AnimationUtils.loadAnimation(requireContext(), R.anim.fade_in)
                    .doOnAnimation(doOnEnd = {
                        isAnimationShown = true
                        binding.mainConstrainLayoutContainer.visibility = View.VISIBLE
                    })
            )
        } else {
            binding.mainConstrainLayoutContainer.visibility = View.VISIBLE
        }
    }
}