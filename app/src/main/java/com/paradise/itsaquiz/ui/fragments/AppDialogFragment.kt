package com.paradise.itsaquiz.ui.fragments

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.paradise.itsaquiz.R
import kotlinx.android.synthetic.main.app_dialog.view.*

class AppDialogFragment : DialogFragment() {

    private val args: AppDialogFragmentArgs by navArgs()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(requireContext()).inflate(R.layout.app_dialog, null, false)
        return MaterialAlertDialogBuilder(requireContext())
            .setView(view)
            .create()
            .also {
                with(view) {
                    titleTextView.text = args.title
                    contentTextView.text = args.content
                    closeImageButton.setOnClickListener { findNavController().popBackStack() }
                }
            }
    }
}