package com.paradise.itsaquiz.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.paradise.itsaquiz.db.converters.TimeStampConverter
import com.paradise.itsaquiz.db.dao.YesNoEntity
import com.paradise.itsaquiz.db.dao.YesNoEntityDao

@TypeConverters(value = [TimeStampConverter::class])
@Database(entities = [YesNoEntity::class], version = 1)
abstract class ItsQuizDataBase : RoomDatabase() {

    abstract fun yesNoEntityDao(): YesNoEntityDao
}
