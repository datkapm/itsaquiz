package com.paradise.itsaquiz.db.dao

import android.os.Parcelable
import android.util.Log
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Transaction
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.gson.annotations.SerializedName
import com.paradise.itsaquiz.db.converters.TimeStamp
import kotlinx.android.parcel.Parcelize
import kotlinx.coroutines.flow.Flow
import java.lang.IllegalStateException

const val TABLE_NAME_YES_NO_ENTITY = "YesNoEntity"

@Dao
interface YesNoEntityDao {

    @Query("SELECT * FROM $TABLE_NAME_YES_NO_ENTITY ORDER BY id DESC")
    fun getAll(): Flow<List<YesNoEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(yesNoEntity: YesNoEntity)

    @Query("SELECT * FROM $TABLE_NAME_YES_NO_ENTITY WHERE id = :entityId")
    suspend fun getYesNoEntityById(entityId: Int): YesNoEntity?

    @Query("SELECT id FROM $TABLE_NAME_YES_NO_ENTITY ORDER BY id DESC LIMIT (:limit) OFFSET (:offset)")
    suspend fun getNumById(offset: Int, limit: Int): List<Int>

    @Query("DELETE FROM $TABLE_NAME_YES_NO_ENTITY WHERE id IN (:entityIds)")
    suspend fun deleteById(entityIds: List<Int>)

    @Transaction
    suspend fun saveYesNoEntityCards(
        offset: Int,
        limit : Int,
        yesNoEntityList: List<YesNoEntity>
    ) {
        fun registerNonFatalException(entity: YesNoEntity) {
            FirebaseCrashlytics.getInstance().recordException(IllegalStateException("Insert data error, entity = $entity"))
        }

        fun isNeedToUpdate(oldEntity: YesNoEntity?, newEntity: YesNoEntity): Boolean {
            return kotlin.runCatching { oldEntity == null || newEntity.lastModified.seconds > oldEntity.lastModified.seconds }
                .onFailure { registerNonFatalException(newEntity) }.getOrElse { false }
        }
        if (yesNoEntityList.isNotEmpty()) {
            yesNoEntityList.filter { entity -> isNeedToUpdate(oldEntity = getYesNoEntityById(entity.id), newEntity = entity) }
                .forEach { entity -> runCatching { insert(entity) }.onFailure { registerNonFatalException(entity) } }

            val idListInDb = getNumById(offset, limit)
            yesNoEntityList.map { it.id }
                .let { yesNoEntityIdList -> idListInDb.filter { yesNoEntityIdList.contains(it).not() } }
                .also {
                    Log.d("AOP","delete $it")
                    deleteById(it) }
        }
    }
}

@Parcelize
@Entity(tableName = TABLE_NAME_YES_NO_ENTITY)
data class YesNoEntity(
    @SerializedName(value = "id") @PrimaryKey val id: Int,
    @SerializedName(value = "title") val title: String,
    @SerializedName(value = "question") val question: String,
    @SerializedName(value = "answer") val answer: String,
    @SerializedName(value = "averageTime") val averageTime: Int,
    @SerializedName(value = "difficulty") val difficulty: String,
    @SerializedName(value = "image") val image: String,
    @SerializedName(value = "lastModified") val lastModified: TimeStamp,
    @SerializedName(value = "isLocked") val isUnlocked: Boolean
) : Parcelable {

    enum class Difficulty { EASY, MEDIUM, HARD }
}
