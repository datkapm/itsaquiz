package com.paradise.itsaquiz.db.converters

import android.os.Parcelable
import androidx.room.TypeConverter
import com.paradise.itsaquiz.firestoreapi.GsonFactory
import kotlinx.android.parcel.Parcelize

class TimeStampConverter {

    @TypeConverter
    fun fromString(json: String): TimeStamp = GsonFactory.yesNoGson.fromJson(json, TimeStamp::class.java)

    /** Конвертирует в json из feed.*/
    @TypeConverter
    fun toJson(time: TimeStamp): String = GsonFactory.yesNoGson.toJson(time)
}

@Parcelize
data class TimeStamp(val seconds: Long, val nanoseconds: Int) : Parcelable
