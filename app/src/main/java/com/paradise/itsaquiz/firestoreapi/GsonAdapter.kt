package com.paradise.itsaquiz.firestoreapi

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.paradise.itsaquiz.db.converters.TimeStamp

//TODO придумать что с этим сделать

class GsonFactory {
    companion object {
        val yesNoGson: Gson = GsonBuilder().registerTypeAdapter(TimeStamp::class.java, yesNoTimeStampAdapter).create()
    }
}

val yesNoTimeStampAdapter = JsonDeserializer { json, _, _ ->
    val jsonObject = json.asJsonObject
    return@JsonDeserializer TimeStamp(jsonObject.get("seconds").asLong, jsonObject.get("nanoseconds").asInt)
}

val yesNoTimeStampAdapterRetrofit = JsonDeserializer { element, _, _ ->
    return@JsonDeserializer TimeStamp(element.asLong, 0)
}

