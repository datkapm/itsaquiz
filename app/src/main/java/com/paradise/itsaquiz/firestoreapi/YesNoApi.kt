package com.paradise.itsaquiz.firestoreapi

import com.paradise.itsaquiz.common.ParcelableResponse
import com.paradise.itsaquiz.db.dao.YesNoEntity
import com.paradise.itsaquiz.network.apiSpecs.YesNoApiSpec

class YesNoApi(private val yesNoApiSpec: YesNoApiSpec) {

    suspend fun getNextPageData(lastLoadedPosition: Int, paginationStep: Int): ParcelableResponse<List<YesNoEntity>> {
        return kotlin.runCatching { yesNoApiSpec.fetchYesNoCards(lastLoadedPosition, paginationStep) }
            .fold(onSuccess = { ParcelableResponse.success(it) }, onFailure = { ParcelableResponse.failure(it) })
    }
}
