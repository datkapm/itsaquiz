package com.paradise.itsaquiz

import android.app.Application
import androidx.room.Room
import com.paradise.itsaquiz.db.ItsQuizDataBase
import com.paradise.itsaquiz.di.component.AppComponent
import com.paradise.itsaquiz.di.component.DaggerAppComponent
import com.paradise.itsaquiz.di.module.AppModule
import com.paradise.itsaquiz.di.module.DaoModule

open class App : Application() {

    val appComponent: AppComponent by lazy(LazyThreadSafetyMode.NONE) {
        val dataBase = Room.databaseBuilder(applicationContext, ItsQuizDataBase::class.java, "itsQuizDataBase").build()
        DaggerAppComponent.builder().appModule(AppModule(context = applicationContext, dataBase = dataBase))
            .daoModule(DaoModule(yesNoEntityDao = dataBase.yesNoEntityDao()))
            .build()
    }
}
