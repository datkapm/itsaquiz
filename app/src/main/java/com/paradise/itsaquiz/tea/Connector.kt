package com.paradise.itsaquiz.tea

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import androidx.savedstate.SavedStateRegistryOwner
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.filter
import com.paradise.itsaquiz.tea.feature.Feature
import com.paradise.itsaquiz.tea.feature.FeatureParams

/**
 * Враппер для [Feature] с сохранением состояния и обработкой ЖЦ.
 */
class Connector<Model : Parcelable, Msg : Any, Props : Any>(
    featureParams: FeatureParams<Model, Msg, Props>,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val feature: Feature<Model, Msg, Props>

    val props: Flow<Props> get() = feature.props

    private val _render = MutableLiveData<Props>()

    @Deprecated(
        message = "Use connector.props or connector.render(lifecycleOwner) {}",
        level = DeprecationLevel.ERROR
    )
    val render: LiveData<Props>
        get() = _render

    init {
        feature = Feature(
            previousModel = savedStateHandle.get(MODEL_KEY),
            init = featureParams.init,
            update = featureParams.update,
            view = featureParams.view,
            featureScope = viewModelScope,
            onEachModel = {
                savedStateHandle.set(MODEL_KEY, it)
                featureParams.onEachModel?.invoke(it)
            }
        )
    }

    override fun onCleared() {
        super.onCleared()
        feature.dispose()
    }

    /** @see Feature.dispatch */
    infix fun dispatch(msg: Msg) = feature.dispatch(msg)

    /** Фабрика вьюмодели для передачи [featureParams] в [Connector]. */
    class Factory<Model : Parcelable, Msg : Any, Props : Any>(
        owner: SavedStateRegistryOwner,
        defaultArgs: Bundle? = null,
        private val featureParams: () -> FeatureParams<Model, Msg, Props>
    ) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {

        @Suppress(names = ["UNCHECKED_CAST"])
        override fun <T : ViewModel> create(key: String, modelClass: Class<T>, handle: SavedStateHandle): T =
            Connector(featureParams(), handle) as T
    }

    private companion object {
        private const val MODEL_KEY = "android_connector"
    }
}

/** Рендерит состояние с учётом жц. */
inline fun <Model : Parcelable, Msg : Any, Props : Any> Connector<Model, Msg, Props>.render(
    fragment: Fragment,
    debounceTime: Long = 0,
    crossinline block: (Props) -> Unit
) =
    props.collectWithLifecycle(fragment.viewLifecycleOwner, debounceTime, block = block)

/** Коллектинг данных с учётом жц. */
@OptIn(FlowPreview::class)
inline fun <T> Flow<T>.collectWithLifecycle(lifecycleOwner: LifecycleOwner, debounceTime: Long = 0, crossinline block: (T) -> Unit) {
    lifecycleOwner.lifecycleScope.launchWhenStarted {
        val viewLifecycle = lifecycleOwner.lifecycle
        this@collectWithLifecycle
            .filter { viewLifecycle.currentState >= Lifecycle.State.STARTED }
            .let { if (debounceTime > 0) it.debounce(debounceTime) else it }
            .collect { block(it) }
    }
}
