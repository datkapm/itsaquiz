package com.paradise.itsaquiz.tea

import kotlinx.coroutines.CoroutineScope
import oolong.Next

/**
 * Создать дефолтное состояние и запустить стартовые эффекты.
 * Если запуск происходит впервые, то `previous = null`.
 * Если же система восстанаваливается после смерти процесса и предыдущее состояние было сохранено,
 * то `previous != null`.
 *
 * Пример использования:
 * ```
 * val init: InitWithPrevious<UserModel, Msg> = { previous ->
 *     val defaultEffects = listOf<Effect<Msg>>(
 *         effect(Msg.StartAnimation),
 *         effect(Msg.UpdateProgress)
 *     )
 *     if (previous != null) {
 *         previous to batch(defaultEffects)
 *     } else {
 *         UserModel(
 *             name = null,
 *             photo = null
 *         ) to batch(
 *             defaultEffects,
 *             effect { dispatch ->
 *                 repo.loadUserInfo().fold(
 *                     success = { userInfo -> dispatch(Msg.UserInfoWasLoaded(userInfo) },
 *                     error = { cause -> dispatch(Msg.Error(cause) }
 *                 )
 *             }
 *         )
 *     }
 * ```
 */
typealias InitWithPrevious<Model, Msg> = (previous: Model?) -> Next<Model, Msg>

/**
 * Приостанавливаемая лямбда. Может быть передан в корутин-билдер.
 *
 * Пример:
 * ```
 * val helloWorld: Suspendable<String> = {
 *     val helloAsync = async { "Hello" }
 *     val worldAsync = async { "World" }
 *     "${helloAsync.await()}" ${worldAsync.await()}!"
 * }
 * launch(helloWorld)
 * ```
 */
typealias Suspendable<T> = suspend CoroutineScope.() -> T
