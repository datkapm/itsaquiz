package com.paradise.itsaquiz.tea

import android.os.Bundle
import android.os.Parcelable
import androidx.activity.ComponentActivity
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelLazy
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import androidx.savedstate.SavedStateRegistryOwner
import com.paradise.itsaquiz.tea.feature.FeatureParams
import kotlin.reflect.KClass

/**
 * Создать [Connector] с восстановлением предыдущего стейта (если он был).
 * @see viewModels
 * @see FeatureParams
 * @see InitializationOptions
 */
fun <Model : Parcelable, Msg : Any, Props : Any> Fragment.androidConnectors(
    defaultArgs: Bundle? = null,
    key: String? = null,
    featureParams: () -> FeatureParams<Model, Msg, Props>,
    storeProducer: () -> ViewModelStore = { viewModelStore },
    initOptions: InitializationOptions = InitializationOptions.OnCreate
): Lazy<Connector<Model, Msg, Props>> =
    createVMLazy(
        featureParams = featureParams,
        defaultArgs = defaultArgs,
        key = key,
        storeProducer = storeProducer,
        initOptions = initOptions
    )

/**
 * Создать [Connector] с восстановлением предыдущего стейта (если он был).
 * @see viewModels
 * @see FeatureParams
 * @see InitializationOptions
 */
fun <Model : Parcelable, Msg : Any, Props : Any> Fragment.sharedAndroidConnectors(
    defaultArgs: Bundle? = null,
    key: String? = null,
    featureParams: () -> FeatureParams<Model, Msg, Props>,
    storeProducer: () -> ViewModelStore = { requireActivity().viewModelStore },
    initOptions: InitializationOptions = InitializationOptions.OnCreate
): Lazy<Connector<Model, Msg, Props>> =
    createVMLazy(
        featureParams = featureParams,
        defaultArgs = defaultArgs,
        key = key,
        storeProducer = storeProducer,
        initOptions = initOptions
    )

/**
 * Создать [Connector] с восстановлением предыдущего стейта (если он был).
 * @see viewModels
 * @see FeatureParams
 * @see InitializationOptions
 */
fun <Model : Parcelable, Msg : Any, Props : Any> ComponentActivity.androidConnectors(
    defaultArgs: Bundle? = null,
    key: String? = null,
    featureParams: () -> FeatureParams<Model, Msg, Props>,
    initOptions: InitializationOptions = InitializationOptions.OnCreate
): Lazy<Connector<Model, Msg, Props>> =
    createVMLazy(
        featureParams = featureParams,
        defaultArgs = defaultArgs,
        key = key,
        storeProducer = { viewModelStore },
        initOptions = initOptions
    )

/** Общий метод для создания коннектора. */
inline fun <reified VM : ViewModel, Model : Parcelable, Msg : Any, Props : Any> SavedStateRegistryOwner.createVMLazy(
    noinline featureParams: () -> FeatureParams<Model, Msg, Props>,
    noinline storeProducer: () -> ViewModelStore,
    key: String?,
    defaultArgs: Bundle?,
    initOptions: InitializationOptions
): Lazy<VM> {
    val factory = Connector.Factory(this, defaultArgs, featureParams)
    return withOptions(
        initOptions = initOptions,
        lazyObj = VMLazy(VM::class, storeProducer, key) { factory }
    )
}

/** Вариация [ViewModelLazy] с одним отличием: можно добавить кастомный [key]. */
class VMLazy<VM : ViewModel>(
    private val viewModelClass: KClass<VM>,
    private val storeProducer: () -> ViewModelStore,
    private val key: String?,
    private val factoryProducer: () -> ViewModelProvider.Factory
) : Lazy<VM> {
    private var cached: VM? = null

    override val value: VM
        get() {
            val viewModel = cached
            return if (viewModel == null) {
                val factory = factoryProducer()
                val store = storeProducer()
                val vmKey = key ?: defaultKey()
                ViewModelProvider(store, factory).get(vmKey, viewModelClass.java).also {
                    cached = it
                }
            } else {
                viewModel
            }
        }

    /**
     * Я не придумал это сам, это копи-паст дефолтной реализации андроида.
     * @see ViewModelProvider.get
     */
    private fun defaultKey(): String {
        val canonicalName = viewModelClass.java.canonicalName
            ?: throw IllegalArgumentException("Local and anonymous classes can not be ViewModels")
        return "androidx.lifecycle.ViewModelProvider.DefaultKey:$canonicalName"
    }

    override fun isInitialized() = cached != null
}

/** Применяет [InitializationOptions] к [lazyObj]. */
fun <VM : ViewModel> LifecycleOwner.withOptions(
    initOptions: InitializationOptions,
    lazyObj: Lazy<VM>
): Lazy<VM> =
    when (initOptions) {
        InitializationOptions.Lazy -> lazyObj
        InitializationOptions.OnCreate -> lazyObj.also {
            lifecycle.addObserver(LifecycleEventObserver { _, event ->
                if (event == Lifecycle.Event.ON_CREATE) {
                    lazyObj.value
                }
            })
        }
    }

/**
 * Варианты инициализации [androidConnectors].
 * [Lazy] - Коннектор будет инициализирован только при обращении.
 * [OnCreate] - Коннектор будет инициализирован когда произойдёт [Lifecycle.Event.ON_CREATE].
 */
enum class InitializationOptions {
    Lazy,
    OnCreate
}
