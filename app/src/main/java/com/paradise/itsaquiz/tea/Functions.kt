package com.paradise.itsaquiz.tea

import oolong.Effect
import oolong.effect

/**
 * Создать эффект из [Suspendable] блока.
 *
 * Пример:
 * ```
 * val suspendable: Suspendable<Msg> = {
 *     val userInfo = repo.loadUserInfo()
 *     Msg.UserInfoHasBeenLoaded(userInfo)
 * }
 * val effect: Effect<Msg> = effectFrom(suspendable)
 * ```
 */
inline fun <Msg : Any> effectFrom(crossinline suspendable: Suspendable<Msg>): Effect<Msg> =
    effect { dispatch -> dispatch(suspendable()) }

/**
 * Оборнуть месседж в эффект.
 *
 * Пример:
 * ```
 * val msg: Msg = Msg.ObserveUserInfo
 * val effect: Effect<Msg> = effect(msg)
 * ```
 */
fun <Msg : Any> effect(msg: Msg): Effect<Msg> =
    effect { dispatch -> dispatch(msg) }
