package com.paradise.itsaquiz.tea.feature

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.launch
import oolong.Dispatch
import oolong.Effect
import oolong.Update
import oolong.View
import com.paradise.itsaquiz.tea.InitWithPrevious

/**
 * Центральный класс, который менеджерит все сущности и запускает весь механизм обработки.
 * Аналог `Oolong.runtime`.
 *
 * [Model] - общий стейт, [Props] - view стейт, [Msg] - сообщения, с помощью которых мы будем трансформировать стейт.
 * @param previousModel предыдущее состояние. Не `null` в том случае, если процесс был убит системой и восстановлен с
 * предыдущим состоянием.
 * @param init лямбда [InitWithPrevious], которая создаёт дефолтный стейт и дефолтный эффект.
 * @param update лямбда [Update], в которой мы реагируем на месседжи и обновляем модель.
 * @param view лямбда [View], которая маппит [Model] в [Props].
 * @param featureScope главный скоуп, на котором будут запускаться все корутины.
 * @param onEachModel коллбек, который мы будем дёргать при каждом обновлении модели.
 * @param effectContext контекст, в котором будут запускаться эффекты.
 * @param renderContext контекст, в котором мы будем рендерить интерфейс.
 */
@OptIn(ExperimentalCoroutinesApi::class)
class Feature<Model : Any, Msg : Any, Props : Any>(
    previousModel: Model?,
    init: InitWithPrevious<Model, Msg>,
    private val update: Update<Model, Msg>,
    private val view: View<Model, Props>,
    featureScope: CoroutineScope,
    private val onEachModel: Dispatch<Model>,
    private val effectContext: CoroutineDispatcher = Dispatchers.Default,
    private val renderContext: CoroutineDispatcher = Dispatchers.Main
) : CoroutineScope by featureScope {
    private var running: Boolean = true

    private val messageChannel = Channel<Msg>(capacity = Channel.BUFFERED)
    private val _states: MutableStateFlow<Model>

    /** [Flow] [Props], которые нужно отрендерить на экране. */
    val props: Flow<Props>
        get() = _states
            .mapLatest { view(it) }
            .flowOn(Dispatchers.Default)

    init {
        val (defaultModel, startEffect) = init(previousModel)
        _states = MutableStateFlow(defaultModel)
        schedule(startEffect)
        launch(effectContext) { start(defaultModel) }
    }

    private suspend fun start(defaultModel: Model) {
        var currentState: Model = defaultModel
        for (msg in messageChannel) {
            val (newState, effect) = update(msg, currentState)
            if (newState != currentState) {
                currentState = newState
                _states.value = newState.also(onEachModel)
            }
            schedule(effect)
        }
    }

    /** Обработка сообщений, запуск сайдэффектов и обновление состояния экрана. */
    fun dispatch(msg: Msg) {
        if (running) sendToMessageChannel(msg)
    }

    private fun sendToMessageChannel(msg: Msg) {
        if (!messageChannel.offer(msg)) {
            error("Msg unhandled - $msg")
        }
    }

    /** Завершить обработку сообщений. */
    fun dispose() {
        running = false
        messageChannel.cancel()
    }

    private fun schedule(effect: Effect<Msg>) =
        launch(effectContext) { effect(::sendToMessageChannel) }
}
