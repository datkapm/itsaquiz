package com.paradise.itsaquiz.tea.feature

import oolong.Dispatch
import oolong.Update
import oolong.View
import com.paradise.itsaquiz.tea.InitWithPrevious

/**
 * Необходимые параметры для создания [Feature].
 * @param onEachModel коллбек, в который Вы можете положить необходимое действие при каждом изменении модели.
 * Например, принт модели в лог, отправка аналитики или запись в префы, чтобы потом восстановить в любой момент, даже
 * если пользователь уйдёт с экрана.
 * @see InitWithPrevious
 * @see Update
 * @see View
 */
class FeatureParams<Model : Any, Msg : Any, Props : Any>(
    val init: InitWithPrevious<Model, Msg>,
    val update: Update<Model, Msg>,
    val view: View<Model, Props>,
    val onEachModel: Dispatch<Model>? = null
)
