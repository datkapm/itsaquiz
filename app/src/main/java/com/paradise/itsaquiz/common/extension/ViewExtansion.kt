package com.paradise.itsaquiz.common.extension

import android.view.View
import androidx.core.view.ViewCompat

var View.transitionNameCompat: String?
    get() = ViewCompat.getTransitionName(this)
    set(value) = ViewCompat.setTransitionName(this, value)
