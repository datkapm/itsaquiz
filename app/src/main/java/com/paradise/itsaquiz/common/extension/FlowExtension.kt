package com.paradise.itsaquiz.common.extension

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.*

@InternalCoroutinesApi
fun <T> Flow<T>.throttleFirst(windowDurationInMillis: Long): Flow<T> = flow {
    var windowStartTime = System.currentTimeMillis()
    var emitted = false
    collect { value ->
        val currentTime = System.currentTimeMillis()
        val delta = currentTime - windowStartTime
        if (delta >= windowDurationInMillis) {
            windowStartTime += delta / windowDurationInMillis * windowDurationInMillis
            emitted = false
        }
        if (!emitted) {
            emit(value)
            emitted = true
        }
    }
}

/** Коллектинг данных с учётом жц. */
@OptIn(FlowPreview::class)
inline fun <T> Flow<T>.collectWithLifecycle(lifecycleOwner: LifecycleOwner, debounceTime: Long = 0, crossinline block: (T) -> Unit) {
    lifecycleOwner.lifecycleScope.launchWhenStarted {
        val viewLifecycle = lifecycleOwner.lifecycle
        this@collectWithLifecycle
            .filter { viewLifecycle.currentState >= androidx.lifecycle.Lifecycle.State.STARTED }
            .let { if (debounceTime > 0) it.debounce(debounceTime) else it }
            .collect { block(it) }
    }
}
