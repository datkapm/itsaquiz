package com.paradise.itsaquiz.common.extension

import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavHost

fun Fragment.findRootNavController(): NavController {
    val activity = requireActivity()
    return if (activity is NavHost) activity.navController else error("Activity $activity must implement NavHost")
}
