package com.paradise.itsaquiz.common

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

sealed class ParcelableResponse<out T> : Parcelable {

    /** Состояние Loading.*/
    @Parcelize
    object Loading : ParcelableResponse<Nothing>()

    /** Состояние Success.*/
    @Parcelize
    data class Success<out T>(val value: @RawValue T) : ParcelableResponse<T>()

    /** Состояние Failure.*/
    @Parcelize
    data class Failure(val error: Throwable) : ParcelableResponse<Nothing>()

    companion object {

        /** Функция возвращает состояние загрузки Loading.*/
        fun <T> loading(): ParcelableResponse<T> = Loading

        /** Функция возвращает состояние загрузки Success.*/
        fun <T> success(value: T): ParcelableResponse<T> = Success(value)

        /** Функция возвращает состояние загрузки Failure.*/
        fun <T> failure(error: Throwable): ParcelableResponse<T> = Failure(error)
    }
}
