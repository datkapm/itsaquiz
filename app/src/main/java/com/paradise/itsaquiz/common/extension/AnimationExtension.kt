package com.paradise.itsaquiz.common.extension

import android.view.animation.Animation

/**
 * Выполняет указанные действия при старте и окончании анимации.
 * @param doOnStart - действие при начале анимации.
 * @param doOnEnd - действие после окончания анимации.
 */
fun Animation.doOnAnimation(doOnStart: (() -> Unit)? = null, doOnEnd: (() -> Unit)? = null): Animation {
    setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(animation: Animation?) { /*nothing*/}

        override fun onAnimationEnd(animation: Animation?) {
            doOnEnd?.invoke()
            setAnimationListener(null)
        }

        override fun onAnimationStart(animation: Animation?) {
            doOnStart?.invoke()
        }
    })

    return this
}
