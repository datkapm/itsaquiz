package com.paradise.itsaquiz.common

/** Proxy тип [Flow], для настройки расширений [Type]. */
internal data class FlowProxy<Type : Any>(val baseType: Type)

/** Предоставляет доступ к расширениям [FlowProxy] для типа [Type]. */
internal val <Type : Any> Type.flow: FlowProxy<Type> get() = FlowProxy(baseType = this)